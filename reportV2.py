from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import csv
from contacts.nomes import pessoas


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive.activity.readonly']

report = []


def removeBadChar(temp = 'null'):
	temp = temp.replace('\u0303','')
	temp = temp.replace('\u2206','')
	temp = temp.replace('\u0302','')
	temp = temp.replace('\u0301','')
	temp = temp.replace('\u0327','')
	temp = temp.replace('\u0308','')
	temp = temp.replace('\u200e','')
	temp = temp.replace('\u2011','')
	
	return temp


def main():
	"""Shows basic usage of the Drive Activity API.

	Prints information about the last 10 events that occured the user's Drive.
	"""
	creds = None
	# The file token.pickle stores the user's access and refresh tokens, and is
	# created automatically when the authorization flow completes for the first
	# time.
	if os.path.exists('token.pickle'):
		with open('token.pickle', 'rb') as token:
			creds = pickle.load(token)
	# If there are no (valid) credentials available, let the user log in.
	if not creds or not creds.valid:
		if creds and creds.expired and creds.refresh_token:
			creds.refresh(Request())
		else:
			flow = InstalledAppFlow.from_client_secrets_file(
				'credentials.json', SCOPES)
			creds = flow.run_local_server(port=0)
		# Save the credentials for the next run
		with open('token.pickle', 'wb') as token:
			pickle.dump(creds, token)

	service = build('driveactivity', 'v2', credentials=creds)
	# Call the Drive Activity API
	results = service.activity().query(body={
		'pageSize':1000
		}).execute()
	activities = results.get('activities', [])

	if not activities:
		print('No activity.')
	else:
		print('Recent activity:')
		for activity in activities:
			time = getTimeInfo(activity)
			action = getActionInfo(activity['primaryActionDetail'])
			actors = truncated(list(map(getActorInfo, activity['actors'])))
			targets = truncated(list(map(getTargetInfo, activity['targets'])))[11:-1]
			report.append([
				time[8:10]+'/'+time[5:7]+'/'+time[:4]+' '+time[11:16],
				actors,
				action,
				removeBadChar(targets)
			])
			print(u'{0}: {1}, {2}, {3}'.format(time, actors, action, targets))
		creatReport(report)


# Returns a string representation of the first elements in a list.
def truncated(array, limit=2):
	contents = ', '.join(array[:limit])
	more = '' if len(array) <= limit else ', ...'
	return u'{0}{1}'.format(contents, more)


# Returns the name of a set property in an object, or else "unknown".
def getOneOf(obj):
	for key in obj:
		return key
	return 'unknown'


# Returns a time associated with an activity.
def getTimeInfo(activity):
	if 'timestamp' in activity:
		return activity['timestamp']
	if 'timeRange' in activity:
		return activity['timeRange']['endTime']
	return 'unknown'


# Returns the type of action.
def getActionInfo(actionDetail):
	return getOneOf(actionDetail)


# Returns user information, or the type of user if not a known user.
def getUserInfo(user):
	if 'knownUser' in user:
		knownUser = pessoas.get(user['knownUser']['personName'])
		if(knownUser == None):
			knownUser = user['knownUser']['personName']
		return knownUser
		#return u'people/me' if isMe else knownUser['personName']
	return getOneOf(user)


# Returns actor information, or the type of actor if not a user.
def getActorInfo(actor):
	if 'user' in actor:
		return getUserInfo(actor['user'])
	return getOneOf(actor)


# Returns the type of a target and an associated title.
def getTargetInfo(target):
	if 'driveItem' in target:
		title = target['driveItem'].get('title', 'unknown')
		return 'driveItem:"{0}"'.format(title)
	if 'drive' in target:
		title = target['drive'].get('title', 'unknown')
		return 'drive:"{0}"'.format(title)
	if 'fileComment' in target:
		parent = target['fileComment'].get('parent', {})
		title = parent.get('title', 'unknown')
		return 'fileComment:"{0}"'.format(title)
	return '{0}:unknown'.format(getOneOf(target))

def creatReport(rep):
	with open('output2.csv', 'w') as csvfile:
		filewriter = csv.writer(csvfile, delimiter=';',lineterminator = '\n',quotechar='|', quoting=csv.QUOTE_MINIMAL)
		filewriter.writerow(['time', 'name','Event','target'])
		for linha in range (len(rep)):
			filewriter.writerow(rep[linha])


if __name__ == '__main__':
	main()