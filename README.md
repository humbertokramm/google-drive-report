# Drive Activity

Este repositório destina-se a salvar o relatório de atividades do google drive para um arquivo ```output.csv```


Leia a documetação no link abaixo para instalar e habilitar a API do Google Drive

https://developers.google.com/drive/activity/v2/quickstart/python


##### Relatório de uma pasta específica:

Acesse a pasta preterida no seu navegador e copie apenas o hash
https://drive.google.com/drive/u/1/folders/```111112222233333444444555556666777```

No arquivo ```folder.py``` insira apenas o hash na forma de string no array com a seguinte ordem: nome da pasta e hash. Para monitorar o relatório do drive inteiro, use 'root' no lugar do hash. Para pegar o relatório de varias pastas, pegue o hash de cada uma e acrescente no array.
```
folder = [
	['root',
	'root'],
	['pasta específica', # não precisa ser o nome da pasta
	'111112222233333444444555556666777']
]
```